// declare a module
var businessApp = angular.module('businessApp', ['ui.router','ui.bootstrap','ngAnimate']);
// handle configuration [routing]
businessApp.config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider) {
  // for any unmatched or undefined url, redirect to /root
  $urlRouterProvider.otherwise("/root");
  // now set up the states
  $stateProvider
    .state('root', {
      url: "/root",
      controller: 'homeController',
      templateUrl: appendDirRoot("partials/home.html"),
    })
    .state('demo', {
      url: "/demo",
      templateUrl: appendDirRoot("partials/demo.html")
    })
    .state('properties', {
        url: "/properties",
        abstract: true,
        template: '<ui-view/>'
    })
      .state('properties.manage', {
        url: "/manage",
        templateUrl: appendDirRoot("partials/properties.manage.html"),
        controller: 'propertiesManagerController'
      })
      .state('properties.rooms', {
        url: "/:id/rooms",
        templateUrl: appendDirRoot("partials/properties.rooms.html"),
        controller: 'propertiesManagerController'
      })
      .state('properties.create', {
        url: "/create",
        templateUrl: appendDirRoot("partials/properties.create.html")
      })
    .state('test', {
      url: "/test",
      templateUrl: appendDirRoot("partials/test.html"),
      controller: function($scope) {
        $scope.items = ["A", "List", "Of", "Items"];
      }
    })
    .state('state2', {
      url: "/state2",
      templateUrl: "partials/demo.html"
    })
    .state('state2.list', {
      url: "/list",
      templateUrl: "partials/state2.list.html",
      controller: function($scope) {
        $scope.things = ["A", "Set", "Of", "Things"];
      }
    });

    function appendDirRoot(a) {
      var ROOT_APP_DIRECTORY = "../page_apps/portal/";
      return ROOT_APP_DIRECTORY + a ;
    }

}]);
