"use strict";
//  npm modules
const requireJSON = require('require-json-ex');
// export configuration
module.exports = function() {
  //  set app context
	var env = process.env.NODE_ENV || 'development';
	//  initialise global values
	var dbConstants = requireJSON("./json/database_config.json")[env];
	var appConstants = applicationConfig();
	var serviceConstants = requireJSON("./json/service_config.json");
  //  keywords
  var DATABASE = 'database';
  var SERVER   = 'server';
  //  application constants
	var obj = {
		application: {
			url: appConstants[env]['url'],
			host: appConstants[env]['host'] || 'localhost',
			port: Number(appConstants[env]['port']) || 5000,
			secret: 'zmK4UOQVRq85dzQB7D197zYxEhsWvJIVTiNEVupCKoyNLccHG4GBruZVBfGWOcFvlhZ4Bf'
		},
		server: {
			host: dbConstants[SERVER]['host'],
      port: dbConstants[SERVER]['port'],
		},
		database: {
      name: dbConstants[DATABASE]['name'],
      username: dbConstants[DATABASE]['username'],
      password: dbConstants[DATABASE]['password']
    },
		services: serviceConstants
	};
  // return object
	return obj;
  //
	function applicationConfig(){
		return {
			'production' : {
				'url' : 'https://' + process.env.NODE_HOST + ':' +
					process.env.NODE_PORT,
				'host' : process.env.NODE_HOST,
				'port' : process.env.NODE_PORT
			},
			'development' : {
				'url' : 'http://' + process.env.NODE_HOST + ':' +
				process.env.NODE_PORT,
				'host' : process.env.NODE_HOST,
				'port' : process.env.NODE_PORT
			},
			'test' : {
				'url' : 'http://' + process.env.NODE_HOST + ':' +
				process.env.NODE_PORT,
				'host' : process.env.NODE_HOST,
				'port' : process.env.NODE_PORT
			}
		};
	}
}();
