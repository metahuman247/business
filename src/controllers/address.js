"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var addressDAO  = require('../dao/address');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function addressController() {};
addressController.prototype = (function() {
	return {
		// [POST] /api/addresses
		insert: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.createAddress(request.payload).then((data) => {
				reply(data).type(headerEnum.json).code(201)
			}).catch((e) => helper.replyError(e));
		},
		// [GET] /api/addresses
		find: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.matchAddress(address)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/addresses/{address_id}
		findByID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.findByIDAddress(request.params.address_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/addresses/{user_id}
		findByByUserID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.findByIDAddress(request.params.address_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/addresses/count
		count: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.countAddresss()
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/addresses/{address_id}
		update: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.update(request.payload)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/addresses/{address_id}/delete
		delete: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.deleteAddress(request.params.address_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/addresses/{address_id}/remove
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			addressDAO.removeAddress(request.params.address_id)
				.then((i)  => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		}
	}
})();
//
module.exports = new addressController();
