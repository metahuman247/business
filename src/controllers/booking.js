"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var bookingDAO  = require('../dao/booking');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function bookingController() {};
bookingController.prototype = (function() {
	return {
		// create a booking
		insert: function insert(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.insert(request.payload).then((data) => {
				reply(data).type(headerEnum.json).code(201)
			}).catch((e) => helper.replyError(e));
		},
		// find a booking
		find: function find(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.findBooking(booking)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// retrieve booking by id
		findByID: function findByID(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.findByIDBooking(request.params.booking_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// count bookings
		count: function find(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.countBookings()
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/bookings/{booking_id}
		// update a booking
		update: function update(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.update(request.payload)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/bookings/{booking_id}/un_cancel
		// un-cancel a booking
		unCancel: function(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.unCancelBooking(request.params.booking_id)
				.then(() => helper.replyFindOne({
					changed: true
				}))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/bookings/{booking_id}/cancel
		// cancels a booking
		cancel: function(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.cancelBooking(request.params.booking_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/bookings/{booking_id}/ban
		// completes a booking
		complete: function(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.cancelBooking(request.params.booking_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/bookings/{booking_id}/delete
		// marks booking as deleted
		delete: function(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.deleteBooking(request.params.booking_id)
				.then(()   => helper.replyFindOne({	changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/bookings/{booking_id}/remove
		// removes booking from database
		remove: function(request, reply) {
			var helper = new ReplyHelper(request, reply);
			bookingDAO.removeBooking(request.params.booking_id)
				.then((i) => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		}
	}
})();
//
module.exports = new bookingController();
