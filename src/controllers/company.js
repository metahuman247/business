"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var companyDAO  = require('../dao/company');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function companyController() {};
companyController.prototype = (function() {
	return {
		// [POST] /api/companies
		insert: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.createCompany(request.payload).then((data) => {
				reply(data).type(headerEnum.json).code(201)
			}).catch((e) => helper.replyError(e));
		},
		// [GET] /api/companies
		find: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.matchCompany(company)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/companies/{company_id}
		findByID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			console.log(request.params.company_id)
			companyDAO.findByIDCompany(request.params.company_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/companies/{user_id}
		findByByUserID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.findByIDCompany(request.params.company_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/companies/count
		count: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.countCompanys()
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/companies/{company_id}
		update: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.update(request.payload)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/companies/{company_id}
		unCancel: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.unCancelCompany(request.params.company_id)
				.then(()   => helper.replyFindOne({ changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/companies/{company_id}
		cancel: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.cancelCompany(request.params.company_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/companies/{company_id}/delete
		delete: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.deleteCompany(request.params.company_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/companies/{company_id}/remove
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			companyDAO.removeCompany(request.params.company_id)
				.then((i)  => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		}
	}
})();
//
module.exports = new companyController();
