"use strict";
//  NPM MODULES
var _  = require('underscore');
var fs = require('fs');
var Boom = require('boom');
var path = require('path');
var randomstring = require("randomstring");
//  PACKAGES
var fileDAO = require('../dao/file');
var ReplyHelper = require('../controllers/reply-helper');
var cloudIMGService = require('../services/cloudinary');
//  DEFINE CONTROLLER
function fileController() {};
fileController.prototype = (function() {
	return {
		// [GET] /api/files/count
		insert: (request,reply) => {
			var data = request.payload;
			if (data.file) {
				var fileName = data.file.hapi.filename;
				var fileExt = fileName.slice((Math.max(0, fileName.lastIndexOf(".")) || Infinity) + 1);
				var pathFile = path.resolve("./temp/") + "/" + randomstring.generate(7) + '.png';
				var file = fs.createWriteStream(pathFile);
				file.on('error', function(err) {
					Boom.badImplementation('ERROR', err);
				});
				data.file.pipe(file);
				data.file.on('data', function(err) {
					console.log('err')

				})
				data.file.on('end', function(err) {
					cloudIMGService.insertImage(pathFile)
					var ret = {
							filename: data.file.hapi.filename,
							headers: data.file.hapi.headers
						}
						// reply(JSON.stringify(ret));
				})
			}
		},
		// [GET] /api/files/count
		count: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			fileDAO.countBookings()
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/files/{file_id}
		findByID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			fileDAO.findByIDBooking(request.params.file_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/files/{file_id}/remove
		update: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			fileDAO.update(request.payload)
				.then(()   => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/files/{file_id}/delete
		delete: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			fileDAO.deleteBooking(request.params.file_id)
				.then(()   => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/files/{file_id}/remove
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			fileDAO.removeBooking(request.params.file_id)
				.then((i)  => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		}
	}
})();
//
module.exports = new fileController();
