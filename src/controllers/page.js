"use strict";
//  NPM MODULES
var _ = require('underscore');
var fs = require('fs');
var Boom = require('boom');
//  PACKAGES
var userDAO = require('../dao/user');
//  var scanDAO = require('../dao/user');
var constants = require('../config/constants.js');
//  controller list constants
var businessAppControllers = fs.readdirSync('./public/page_apps/business/controllers');
var portalAppControllers   = fs.readdirSync('./public/page_apps/portal/controllers');
//  DEFINE CONTROLLER
function userController(){};
userController.prototype = (function(){
	return {
    home: function (request,reply) {
			var dataPayload =  {
        controllers: businessAppControllers
			}
		  reply.view('business',dataPayload);
		},
		portal: function (request,reply) {
			var dataPayload =  {
				controllers: portalAppControllers
			}
			reply.view('portal',dataPayload);
		},
		register: function (request,reply) {
			var dataPayload =  {

			}
		  reply.view('register',dataPayload);
		},
		login: function find(request,reply) {
			var dataPayload =  {

			}
		  reply.view('login',dataPayload);
		},
		logout: function (request,reply) {
			var dataPayload =  {

			}
			reply.view('logout',dataPayload);
		},
		listingById: function (request,reply) {
			var dataPayload =  {

			}
			var templateId = 'listing_id_by_id';
			reply.view(templateId,{ listing_id:request.params.listing_id });
		}
	}
})();
module.exports = new userController();
