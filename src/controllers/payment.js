"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var paymentDAO  = require('../dao/payment');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function paymentController() {};
paymentController.prototype = (function() {
	return {
		// [POST] /api/payments
		insert: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.createPayment(request.payload).then((data) => {
				reply(data).type(headerEnum.json).code(201)
			}).catch((e) => helper.replyError(e));
		},
		// [GET] /api/payments
		find: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.matchPayment(payment)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/payments/{payment_id}
		findByID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			console.log(request.params.payment_id)
			paymentDAO.findByIDPayment(request.params.payment_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/payments/{user_id}
		findByByUserID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.findByIDPayment(request.params.payment_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/payments/count
		count: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.countPayments()
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/payments/{payment_id}
		update: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.update(request.payload)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/payments/{payment_id}
		unCancel: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.unCancelPayment(request.params.payment_id)
				.then(()   => helper.replyFindOne({ changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/payments/{payment_id}
		cancel: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.cancelPayment(request.params.payment_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/payments/{payment_id}/delete
		delete: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.deletePayment(request.params.payment_id)
				.then(()   => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/payments/{payment_id}/remove
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			paymentDAO.removePayment(request.params.payment_id)
				.then((i)  => helper.replyFindOne({ changed: true }))
				.catch((e) => helper.replyError(e));
		}
	}
})();
//
module.exports = new paymentController();
