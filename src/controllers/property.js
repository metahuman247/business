"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var propertyDAO = require('../dao/property');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function propertyController() {};
propertyController.prototype = (function() {
	return {
		// [POST] /api/properties
		insert: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.insert(request.payload).then((data) => {
				reply(data).type(headerEnum.json).code(201)
			}).catch((e) => helper.replyError(e));
		},
		// [GET] /api/properties
		find: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.matchProperty(property)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/properties/count
		count: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.countProperties()
			.then((x)  => helper.replyFindOne(x))
			.catch((e) => helper.replyError(e));
		},
		// [GET] /api/properties/{property_id}
		findByID: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.findByIDProperty(request.params.property_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [UPDATE] /api/properties/{property_id}
		update: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.update(request.payload)
				.then(()   => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/properties/{property_id}/delete
		delete: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.deleteProperty(request.params.property_id)
				.then(()   => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/properties/{property_id}/remove
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			propertyDAO.removeProperty(request.params.property_id)
				.then((i)  => helper.replyFindOne({changed: true}))
				.catch((e) => helper.replyError(e));
		}
	}
})();
// EXPORT ROUTES
module.exports = new propertyController();
