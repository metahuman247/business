"use strict";
//  NPM MODULES
var _ = require('underscore');
//  PACKAGES
var userDAO = require('../dao/user');
var ReplyHelper = require('../controllers/reply-helper');
//  DEFINE CONTROLLER
function userController(){};
userController.prototype = (function(){
	return {
		// [POST] /api/users
		// inserts a User
		insert: function insert(request,reply) {
			var helper  = new ReplyHelper(request,reply);
			userDAO.createUser(request.payload)
				.then((x)  => { reply(x).type(headerEnum.json).code(201); })
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/users
		// finds Users by creteria
		find: function find(request,reply) {
			var helper   = new ReplyHelper(request,reply);
			userDAO.findByID(params,user)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/users/count
		// counts User's by criteria
		count: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			var critera  = request.query.critera;
			userDAO.countUsers(critera)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/users/{user_id}
		// finds User by its user_id
		findByID: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.findByIDUser(request.params.user_id)
				.then((x)  => helper.replyFindOne(x))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/users/{user_id}/colleagues
		// finds a User's colleagues
		findByIdColleagues: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.findByIdColleagues(request.params.user_id)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [GET] /api/users/{user_id}/teammates
		// finds a User's temmates
		findByIdTeamMates: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.findByIdTeamMates(request.params.user_id)
				.then((xs) => helper.replyFindMany(xs))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/users/{user_id}/ban
		// updates a User
		update: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.update(request.payload)
				.then(() 	 => helper.replyFindOne({changed:true}))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/users/{user_id}/unban
		// removes ban flag from User
		unBan: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.unBanUser(request.params.user_id)
				.then(()   => helper.replyFindOne({changed:true}))
				.catch((e) => helper.replyError(e));
		},
		// [PUT] /api/users/{user_id}/ban
		// marks User as banned
		ban: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.banUser(request.params.user_id)
				.then(() 	 => helper.replyFindOne({changed:true}))
				.catch((e) => helper.replyError(e));
		},
		// [DELETE] /api/users/{user_id}/delete
		// marks User as deleted
		delete: (request,reply) => {
			var helper   = new ReplyHelper(request,reply);
			userDAO.deleteUser(request.params.user_id)
				.then(() 	 => helper.replyFindOne({changed:true}))
				.catch((e) => helper.replyError(e));
		},
	  // [DELETE] /api/users/{user_id}/remove
		// removes a User from database
		remove: (request,reply) => {
			var helper = new ReplyHelper(request,reply);
			userDAO.removeUser(request.params.user_id)
			.then((i)  => helper.replyFindOne({changed:true}))
			.catch((e) => helper.replyError(e));
	 	}
	}
})();
//
module.exports = new userController();
//var params = request.plugins.createControllerParams(request.params);
