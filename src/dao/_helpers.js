// helpers for dao
module.exports = {
  classes: {
    // vertices
    Booking:  'Booking',  // BO | V > $this
    Bill:     'Bill',     // BI | V > $this
    Company:  'Company',  // CO | V > $this
    Payment:  'Payment',  // PA | V > $this
    Picture:  'Picture',  // PI | V > Resource > $this
    Property: 'Property', // PR | V > $this
    Resource: 'Resource', // RE | V > $this > _
    Team:     'Team',     // RE | V > $this
    User:     'User',     // US | V > $this
    Video:    'Video',    // VI | V > Resource > $this
    // edge
    assigned_to: 'assigned_to',
    belongs_to:  'belongs_to',
    booked_by:   'booked_by',
    invited_by:  'invited_by',
    sold_by:     'sold_by',
    paid_towards:'paid_towards',
    purchase_by: 'purchase_by',
    viewed_by:   'viewed_by',
    works_for:   'works_for',
  },
  sls: function(strings) {
    var values = Array.prototype.slice.call(arguments, 1);
    var output = '';
    for (var i = 0; i < values.length; i++) {
      output += strings[i] + values[i];
    }
    output += strings[values.length];
    var lines = output.split(/(?:\r\n|\n|\r)/);
    return lines.map(function(line) {
      return line.replace(/^\s+/gm, '');
    }).join(' ').trim();
  },
  interporlateSQL: function functionName(stmt) {
    return stmt.values.reduce(function(previousValue, currentValue, currentIndex) {
      return previousValue.replace(`$${currentIndex + 1}`,currentValue);
    },stmt.sql);
  }
}
