"use strict";+
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen       = new SqlGenerator();
//  PACKAGES
var db  = require('../middleware/db');
var oDB = db.orientDB
var util = require('./_helpers');
//  DAO CONSTRUCTOR
function AddressDAO() {};
AddressDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  helper values
    var classEnum = util.classes;
    //  dao methods
     return {
        // @return Array[Object]
        insertAddress: function (data) {
            var sql = util.sls `select expand(registerAddress())`;
            return oDB.insert().into(classEnum.Address).set(data).one();
        },
        // @return Array[Object]
        matchAddress: function (criteriaRecord) {
          return oDB.select('*').from(classEnum.Address).where(criteriaRecord).all()
        },
        // @return Array[Object]
        findAddressByID: function (address_id) {
          return oDB.select('*').from(classEnum.Address).where({
            address_id: address_id,
            is_deleted: false
          }).all()
        },
        // @return Array[Object]
        countAppAddresses: function (filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.Address).where(where).all()
        },
        // @return Array[Object]
        countAddressesByUser: function (user_id) {
          return oDB.select('count(*)').from(classEnum.Address).where({user_id:user_id}).all()
        },
        // @return Array[Object]
        countAddressesByCompany: function (company_id) {
          return oDB.select('count(*)').from(classEnum.Address).where({company_id:company_id}).all()
        },
        // @return Array[Object]
        createAddress: function (address_id,data) {
          var sql = util.sls `select expand(registerAddress())`;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Number
        updateAddress: function (address_id, data) {
          var where = {address_id: data.address_id};
          return oDB.update(classEnum.Address).set(data).where({address_id: address_id}).scalar();
        },
        // @return Number
        deleteAddress: function(address_id) {
            return oDB.update(classEnum.Address).set({is_deleted:true}).where({address_id: address_id}).scalar();
        },
        // @return Number
        cancelAddress: function(address_id) {
            var where = filter;
            return oDB.delete().from(classEnum.Address).where({address_id: address_id }).limit(1).scalar()
        }
    };
})();
module.exports = new AddressDAO();
