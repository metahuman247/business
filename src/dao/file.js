"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
//  PACKAGES
var db  = require('../middleware/db');
var oDB = db.orientDB
var helpers = require('./_helpers');
var classEnum = helpers.classes;
//  DEFINE DAO
function FileDAO() {};
FileDAO.prototype = (function() {
    return {
        // @return <Array<Object>
        insertFile: function (data) {
            return oDB.insert().into(classEnum.File).set(data).one();
        },
        // @return <Number>
        countFiles: function (filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.File).where(where).all();
        },
        // @return <Array<Object>
        findByIDFile: function ( file_id) {
          var where = {file_id: file_id};
          return oDB.select('*').from(classEnum.File).where(where).all();
        },
        // @return <Array<Object>
        matchFile: function (rec) {
          return oDB.select('*').from(classEnum.File).where(rec).all();
        },
        // @return <Array<Object>
        updateFile: function (file_id,data) {
          return oDB.update(classEnum.File).set(data).where({file_id: file_id}).scalar();
        },
        // @return Number
        unCancelFile: function (file_id) {
          return oDB.update(classEnum.File).set({is_cancelled:false}).where({file_id: file_id}).scalar();
        },
        // @return Number
        cancelFile: function (file_id) {
          return oDB.update(classEnum.File).set({is_cancelled:true}).where({file_id: file_id}).scalar();
        },
        // @return Number
        deleteFile: function(file_id) {
            var where = filter;
            return oDB.update(classEnum.File).set({is_deleted: true}).where(where).scalar();
        },
        // @return Number
        removeFile: function(params) {
            var where = filter;
            return oDB.delete().from(classEnum.File).where({file_id:file_id }).limit(1).scalar()
        }
    };
})();
module.exports = new FileDAO();
