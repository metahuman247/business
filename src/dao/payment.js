"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen       = new SqlGenerator();
//  PACKAGES
var db   = require('../middleware/db');
var oDB  = db.orientDB
var util = require('./_helpers');
var paymentGen = require('../dao_generators/user');
//  DAO CONSTRUCTOR
function PaymentDAO() {};
PaymentDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  helper values
    var classEnum = util.classes;
    //  dao methods
     return {
        // @return Array[Object]
        insertPayment: function (data) {
          var paymentDataJSON = JSON.stringify(paymentGen.genInitPayment(data));
          var sql = util.sls `select expand(registerPayment(${paymentDataJSON}))`;
          return oDB.insert().into(classEnum.Payment).set(data).one();
        },
        // @return Array[Object]
        matchPayment: function (criteriaRecord) {
          return oDB.select('*').from(classEnum.Payment).where(criteriaRecord).all()
        },
        // @return Array[Object]
        findByIDPayment: function (payment_id,isDeleted,isCancelled) {
          return oDB.select('*').from(classEnum.Payment).where({
            payment_id:  payment_id,
            is_deleted: isDeleted || false,
            is_cancelled: isCancelled || false,
          }).all()
        },
        // @return Array[Object]
        countAppPayments: function (filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.Payment).where(where).all()
        },
        // @return Array[Object]
        countPaymentsByUser: function (user_id) {
          return oDB.select('count(*)').from(classEnum.Payment).where({user_id:user_id}).all()
        },
        // @return Array[Object]
        countPaymentsByCompany: function (company_id) {
          return oDB.select('count(*)').from(classEnum.Payment).where({company_id:company_id}).all()
        },
        // @return Array[Object]
        createPayment: function (payment_id,data) {
          var sql = util.sls `select expand(registerPayment())`;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Number
        updatePayment: function (payment_id, data) {
          var where = {payment_id: data.payment_id};
          return oDB.update(classEnum.Payment).set(data).where({payment_id: payment_id}).scalar();
        },
        // @return Number
        unCancelPayment: function(payment_id) {
            return oDB.update(classEnum.Payment).set({is_cancelled: false}).where({payment_id: payment_id}).scalar();
        },
        // @return Number
        cancelPayment: function(payment_id) {
          var where = filter;
          return oDB.update(classEnum.Payment).set({is_cancelled: true}).where({payment_id: payment_id}).scalar();
        },
        // @return Number
        deletePayment: function(payment_id) {
          return oDB.update(classEnum.Payment).set({is_deleted:true}).where({payment_id: payment_id}).scalar();
        },
        // @return Number
        removePayment: function(payment_id) {
          return oDB.delete().from(classEnum.Payment).where({payment_id: payment_id }).limit(1).scalar()
        }
    };
})();
module.exports = new PaymentDAO();
