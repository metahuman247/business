"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
//  PACKAGES
var db  = require('../middleware/db');
var oDB = db.orientDB;
var helpers = require('./_helpers');
var classEnum = helpers.classes;
//  DEFINE DAO
function PropertyDAO() {};
PropertyDAO.prototype = (function() {
    return {
        // @return <Array<Object>
        insertProperty: function (params,data) {
          return oDB.insert().into(classEnum.Property).set(data).one();
        },
        // @return <Array<Object>
        matchPropertys: function (rec) {
          return oDB.select('*').from(classEnum.Property).where(rec).all();
        },
        // @return <Array<Object>
        findByIDProperty: function (property_id) {
          return oDB.select('*').from(classEnum.Property)
          .where({ property_id: property_id, is_deleted: false}).all();
        },
        // @return <Number>
        countPropertys: function (params,filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.Property).where(where).all();
        },
        // @return <Array<Object>
        updateProperty: function (property_id,data) {
          return oDB.update(classEnum.Property).set(data).where({property_id: property_id}).scalar();
        },
        // @return Number
        unCancelProperty: function (property_id) {
          return oDB.update(classEnum.Property).set({is_cancelled:false}).where({property_id: property_id}).scalar();
        },
        // @return Number
        cancelProperty: function (property_id) {
          return oDB.update(classEnum.Property).set({is_cancelled:true}).where({property_id: property_id}).scalar();
        },
        // @return Number
        deleteProperty: function(property_id) {
            var where = filter;
            return oDB.update(classEnum.Property).set({is_deleted: true}).where(where).scalar();
        },
        // @return Numbers
        removeProperty: function(params) {
            var where = filter;
            return oDB.delete().from(classEnum.Property).where({property_id: property_id }).limit(1).scalar()
        }
    };
})();
module.exports = new PropertyDAO();
