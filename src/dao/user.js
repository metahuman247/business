"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
var objFilter = require('object-key-filter');
//  PACKAGES
var db = require('../middleware/db');
var oDB = db.orientDB
var util = require('./_helpers');
var userGen = require('../dao_generators/user');
//  DAO CONSTRUCTOR
function UserDAO() {};
UserDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  misc vars
    var IMMUTABLE_PROPS = ['user_id','auth_username'];
    //  helper values
    var classEnum = util.classes;
    //  dao methods
    return {
        // @return Array[Object]
        createUser: function (data) {
          var userDataJSON = JSON.stringify(userGen.genInitUser(data));
          var sql = util.sls `select UserCreate(${userDataJSON})`;
          return oDB.query(sql);
        },
        // @return Array[Object]
        matchUsers: function (user_id) {
          return oDB.select('*').from(classEnum.User).where({}).all()
        },
        // @return Array[Object]
        findByIDUser: function (user_id) {
          return oDB.select('*').from(classEnum.User).where({user_id: user_id}).all()
        },
        // @return Array[Object]
        matchUsers: function (matchObj) {
          return oDB.select('*').from('User').where(matchObj).all()
        },
        // @return Array[Object]
        findByUserIdColleagues: function (user_id) {
          // select users working_for companies I am in
          var sql = `SELECT expand(in(works_for)) FROM (SELECT expand(out(works_for)) FROM User WHERE user_id = "${user_id}") `;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Array[Object]
        findUserByUserIdTeamMates: function (user_id,comp_user_id) {
          // select users working_for specifc company I am in
          var sql = util.sls`
              SELECT * FROM (
                SELECT expand(out(${classEnum.works_for})) FROM ${classEnum.User} WHERE auth_username = "${user_id}"
              )
              WHERE user_id = "${comp_user_id}"
              `;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Array[Object]
        findByUserIdManager: function (user_id,comp_user_id) {
          // select users working_for specifc company I am in
          var sql = util.sls`
              SELECT * FROM (
                SELECT expand(out(${classEnum.works_for})) FROM ${classEnum.User} WHERE auth_username = "${user_id}"
              )
              WHERE user_id = "${comp_user_id}"
              `;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Array[Object]
        findByUserIdTeamsMates: function (user_id,comp_user_id) {
          // select users working_for specifc company I am in
          var sql = util.sls` SELECT * FROM
                                (SELECT expand(out(${classEnum.working_for})) FROM ${classEnum.User}
                              WHERE auth_username = "${user_id}" WHERe user_id = "${comp_user_id}"
                            `;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Number
        countUsers: function (filter) {
          return oDB.select('count(*)').from(classEnum.User).all()
        },
        // @return Array[Object]
        insertUser: function ( data) {
            return oDB.insert().into(classEnum.User).set(data).one();
        },
        // @return Array[Object]
        updateUser: function (data) {
          var userData = objFilter(data,IMMUTABLE_PROPS);
          return oDB.update(classEnum.User).set(userData).where(where).scalar();
        },
        // @return Array[Object]
        unBanUser: function(user_id) {
            return oDB.update(classEnum.User).set({is_banned: false}).where({user_id: user_id}).scalar();
        },
        // @return Array[Object]
        banUser: function(user_id) {
            return oDB.update(classEnum.User).set({is_banned: true}).where({user_id: user_id}).scalar();
        },
        // @return Array[Object]
        deleteUser: function(user_id) {
            return oDB.update(classEnum.User).set({is_deleted: true}).where({user_id: user_id}).scalar();
        },
        // @return Number
        removeUser: function(user_id) {
            var where = filter;
            return oDB.delete().from(classEnum.User).where({user_id: user_id}).limit(1).scalar()
        },
    };
})();
module.exports = new UserDAO();
