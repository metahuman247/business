"use strict";
//  import database
var db  = require('../middleware/db');
var oDB = db.orientDB
var util = require('./_helpers');
//  import packages
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator()4;
//  define dao
function VideoDAO() {};
//  define methods
VideoDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  helper values
    var classEnum = util.classes;
    //  define the dao
    return {
        // @return <Object>
        insertVideo: function (params, data) {
            var sql = util.sls `select expand(registerVideo())`;
            return oDB.insert().into(classEnum.Video).set(data).one();
        },
        // @return <Array<Object>
        findVideoByID: function (params, meta_id) {
          var where = {meta_id: meta_id};
          return oDB.select('*').from(classEnum.Video).where(where).all()
        },
        // @return
        findVideoSimple: function (params,stipulations) {
          return oDB.select('*').from(classEnum.Video).where(stipulations).all()
        },
        // @return
        findVideoByListing: function (params,stipulations) {
          return oDB.select('*').from(classEnum.Video).where(stipulations).all()
        },
        // @return
        createVideo: function (payment_id,data) {
          var sql = util.sls `select expand(registerVideo())`;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return
        updateVideo: function (payment_id, data) {
          var where = {meta_id: data.meta_id};
          return oDB.update(classEnum.Video).set(data).where(payment_id: payment_id }).scalar();
        },
        // @return
        deleteVideo: function(payment_id) {
            var where = filter;
            return oDB.update(classEnum.Video).set({is_deleted: true}).where(where).scalar();
        },
        // @return
        cancelVideo: function(payment_id) {
            var where = filter;
            return oDB.delete().from(classEnum.Video).where({payment_id: payment_id }).limit(1).scalar()
        },
    };
})();
module.exports = new VideoDAO();
