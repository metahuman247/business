"use strict";
//  NPM MODULES
var merge = require('tea-merge');
var uuid  = require('uuid');
//  DEFINE GEN
function CompanyGen() {};
CompanyGen.prototype = (function() {
    return {
        // @return <Object>
        genInitCompany: (inputObj) => {
            return merge(genInitDefaults(),inputObj)
        }
    };
})();
//
module.exports = new CompanyGen();
function genInitDefaults (){
  return {
      "amount_payed": null,
      "context": null,
      "coupon":  null,
      "gateway": null,
      "date_payed": format.asString('yyyy-MM-dd hh:mm:ss', new Date()),
      "is_confirmed": false,
      "company_id": uuid.v4()
  }
}
