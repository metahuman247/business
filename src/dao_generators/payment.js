"use strict";
//  NPM MODULES
var merge = require('tea-merge');
var uuid  = require('uuid');
//  DEFINE GEN
function PaymentGen() {};
PaymentGen.prototype = (function() {
    return {
        // @return <Object>
        genInitPayment: (inputObj) => {
            return merge(genInitDefaults(),inputObj)
        }
    };
})();
module.exports = new PaymentGen();
function genInitDefaults (){
  return {
      "amount_payed": null,
      "context": null,
      "coupon":  null,
      "gateway": null,
      "date_payed": format.asString('yyyy-MM-dd hh:mm:ss', new Date()),
      "is_confirmed": false,
      "payment_id": uuid.v4()
  }
}
