"use strict";
//  npm modules
var _ = require('underscore');
var OrientDB = require('orientjs');
var constants = require('../config/constants.js');
// define factory
module.exports = function() {
 var server = OrientDB(constants.server);
 var externals = {};
 externals.orientDB = server.use(constants.database);
 // export drivers
 return externals;
}();
