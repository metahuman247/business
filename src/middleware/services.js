"use strict";
//  NPM MODULES
var _ = require('underscore');
var cloudinaryConfig = require('cloudinary');
var mailGunConfig = require('mailgun-js');
var twilioConfig = require('twilio');
//  PACKAGES
var constants = require('../config/constants.js');
var servicesData = constants.services;
//  DEF FACTORY
module.exports = function() {
 var externals = {};
 // define services
 cloudinaryConfig.config(servicesData["cloudinary"]);
 var mailGun = mailGunConfig(servicesData['mail_gun']);
 var twilio  = twilioConfig(servicesData['twilio']['account_sid'],
                      servicesData['twilio']['auth_token'])
 // bind services to export object
 externals.cloudinary = cloudinaryConfig;
 externals.mailGun = mailGun;
 externals.twilio  = twilio;
 // export drivers
 return externals;
}();
/*
docs:
https://www.npmjs.com/package/mailgun-js
http://twilio.github.io/twilio-node/
*/
