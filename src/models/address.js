"use strict";
//  NODE MODULES
var Joi = require('joi');
//  MODEL DEFINITION
function AddressModel(){
	this.schema = {
    address_id: Joi.string().max(255),
		city: Joi.string().max(255),
		country: Joi.string().max(255),
		postal_code: Joi.array().max(255),
		province: Joi.string().max(255),
    street_address: Joi.string().max(255)
	};
};
// UTILITIES
AddressModel.prototype = (function() {
	return {
	};
})();
module.exports = AddressModel;
