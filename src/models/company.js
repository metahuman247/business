"use strict";
//  NODE MODULES
var Joi = require('joi');
// MODEL DEFINITION
function CompanyModel(){
	this.schema = {
		addresses: Joi.array().max(255),
    company_id: Joi.string().max(255),
		industry: Joi.string().max(255),
		is_deleted: Joi.string().max(255),
		locations: Joi.array().max(255),
		name: Joi.string().max(255)
	};
};
// UTILITIES
CompanyModel.prototype = (function() {
	return {

	};
})();
module.exports = CompanyModel;
