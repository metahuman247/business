"use strict";
//  NODE MODULES
var Joi = require('joi');
//  MODEL DEFINITION
function FileModel(){
	this.schema = {
		amount_payed: Joi.number().max(255),
		context: Joi.string().max(255),
		coupon: Joi.string().max(255),
    is_deleted: Joi.string().max(255),
		date_expiring: Joi.array().max(255),
		date_payed: Joi.string().max(255),
    file_id: Joi.string().max(255)
	};
};
// UTILITIES
FileModel.prototype = (function() {
	return {

	};
})();
module.exports = FileModel;
