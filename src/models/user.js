"use strict";
//  NODE MODULES
var _ = require('underscore');
var Joi = require('joi');
var crypto = require('crypto');
// MODEL DEFINITION
function UserModel(){
	this.schema = {
		auth_email: Joi.string().max(255),
		auth_username: Joi.string().max(255),
		auth_password: Joi.string().max(255),
		date_registered: Joi.string().max(255),
		personal_first_name: Joi.string().max(255),
		personal_last_name: Joi.string().max(255),
		personal_age: Joi.string().max(255),
		personal_sex: Joi.string(1).max(5),
		is_banned: Joi.string().max(255),
		meta_doc_history: Joi.string().max(255),
		meta_date: Joi.string().max(255),
		user_id: Joi.string().max(255),
	};
};
// UTILITIES
UserModel.prototype = (function() {
	return {
		encryptPass: function(password) {
			var salt = '1d098an18da7cn';
			var sha1 = crypto.createHash('sha1').update(password).digest('hex') + salt;
			return crypto.createHash('sha256').update(sha1).digest('hex');
		}
	};
})();
module.exports = UserModel;
