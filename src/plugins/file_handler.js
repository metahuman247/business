"use strict";
//  NPM MODULES
var Path = require('path');
var Inert = require('inert');
var Path = require('path');
//  REGISTER PLUGIN
module.exports = function (server) {
  server.register(Inert, function () {
    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'public'
            }
        }
    });
  });
}
