"use strict";
//  NPM MODULES
var Path = require('path');
var Hoek = require('hoek');
//  REGISTER PLUGIN
module.exports = function (server) {
  server.register(require('vision'), (err) => {
  Hoek.assert(!err, err);
  server.views({
        engines: {
            html: require('handlebars')
        },
        relativeTo: __dirname,
        path: '../templates',
        partialsPath: '../templates/partials'
    });
  });
}
