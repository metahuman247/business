"use strict";
//  NPM MODULES
var bookingController = require('../controllers/booking');
/// var fileValidate   = require('../validate/file');
//  DEFINE ROUTES
module.exports = function() {
	return [
		{ // creates new booking
			method: 'POST',
			path: '/api/bookings',
			config : {
				handler : bookingController.insert,
				//validate : userValidate.insert
			}
		},
		{ // get user's bookings
			method: 'GET',
			path: '/api/bookings',
			config : {
				handler: bookingController.find,
				//validate : userValidate.find
			}
		},
		{ // get specific booking
			method: 'GET',
			path: '/api/bookings/{booking_id}',
			config : {
				handler: bookingController.findByID,
				//validate: userValidate.findByID
			}
		},
		{ // updates booking
			method: 'PUT',
			path: '/api/bookings/{booking_id}',
			config : {
				handler: bookingController.update,
				//validate : userValidate.update
			}
		},
		{ // marks booking as deleted
			method: 'DELETE',
			path: '/api/bookings/{booking_id}/delete',
			config : {
				handler: bookingController.delete,
				//validate : userValidate.delete
			}
		},
		{ // removes booking from db
			method: 'DELETE',
			path: '/api/bookings/{booking_id}/remove',
			config : {
				handler: bookingController.delete,
				//validate : userValidate.delete
			}
		}
	];
}();
