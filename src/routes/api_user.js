"use strict";
//  PACKAGES
var userController = require('../controllers/user');
var userValidate   = require('../validate/user');
//  DEFINE ROUTES
module.exports = function() {
	return [
		{ // create new user
			method: 'POST',
			path: '/api/users',
			config: {
				handler:  userController.insert,
      //  validate: userValidate.insert
			}
		},
		{ // get users list
			method: 'GET',
			path: '/api/users',
			config: {
				handler: userController.find
			}
		},
		{ // get user's count
			method: 'GET',
			path: '/api/users/count',
			config: {
				handler: userController.count
			}
		},
		{ // get specific user
			method: 'GET',
			path: '/api/users/{user_id}',
			config : {
				handler:  userController.findByID,
        validate: userValidate.findByID
			}
		},
		{ // get specific user's colleagues
			method: 'GET',
			path: '/api/users/{user_id}/colleagues',
			config : {
				handler:  userController.findByIdColleagues,
				validate: userValidate.refByID
			}
		},
		{ // get specific user's teammates
			method: 'GET',
			path: '/api/users/{user_id}/teammates',
			config : {
				handler:  userController.findByIdTeamMates,
				validate: userValidate.refByID
			}
		},
		{ // update specific user
			method: 'PUT',
			path: '/api/users/{user_id}',
			config : {
				handler:  userController.update,
				validate: userValidate.update
			}
		},
		{ // unban specific user
			method: 'PUT',
			path: '/api/users/{user_id}/unban',
			config: {
				handler:  userController.unBan,
				validate: userValidate.refByID
			}
		},
		{ // ban user
			method: 'PUT',
			path: '/api/users/{user_id}/ban',
			config: {
				handler:  userController.ban,
				validate: userValidate.refByID
			}
		},
		{ // delete user
			method: 'DELETE',
			path: '/api/users/{user_id}/delete',
			config: {
				handler:  userController.delete,
				validate: userValidate.refByID
			}
		},
		{ // remove specific user from database
			method: 'DELETE',
			path: '/api/users/{user_id}/remove',
			config: {
				handler:  userController.remove,
				validate: userValidate.refByID
			}
		}
	];
}();
