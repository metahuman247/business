"use strict";
//  npm modules
var companyController = require('../controllers/company');
var companyValidate   = require('../validate/company');
//  define routes
module.exports = function() {
	return [
		{ // creates new company
			method: 'POST',
			path: '/api/companies',
			config : {
				handler:  companyController.insert,
				validate: companyValidate.insert
			}
		},
		{ // get user's companies
			method: 'GET',
			path: '/api/companies',
			config : {
				handler:  companyController.find,
				validate: companyValidate.find
			}
		},
		{ // get specific company
			method: 'GET',
			path: '/api/companies/{company_id}',
			config : {
				handler: companyController.findByID,
				validate: companyValidate.refByID
			}
		},
		{ // updates company
			method: 'PUT',
			path: '/api/companies/{company_id}',
			config : {
				handler: companyController.update,
				//validate : companyValidate.update
			}
		},
		{ // marks company as deleted
			method: 'DELETE',
			path: '/api/companies/{company_id}/delete',
			config : {
				handler: companyController.delete,
				validate : companyValidate.refByID
			}
		},
		{ // removes company from db
			method: 'DELETE',
			path: '/api/companies/{company_id}/remove',
			config : {
				handler: companyController.delete,
				validate : companyValidate.refByID
			}
		}
	];
}();
