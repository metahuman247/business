"use strict";
//  PACKAGES
var pageController = require('../controllers/page');
//  PAGE DEFINITIONS
module.exports = function() {
	return [
		{ // main page
			method: 'GET',
			path: '/',
			config : {
				handler : pageController.home
			}
		},
		{ // main page
			method: 'GET',
			path: '/portal',
			config : {
				handler : pageController.portal
			}
		},
		{ // page for registration
			method: 'GET',
			path: '/register',
			config : {
				handler : pageController.register
			}
		},
		{ // page for logging in
			method: 'GET',
			path: '/login',
			config : {
				handler: pageController.login
			}
		},
		{ // page for resetting password
			method: 'GET',
			path: '/forgot_password/{reset_id}',
			config : {
				handler: pageController.login
			}
		},
		{ // scan page
			method: 'GET',
			path: '/listings/{listing_id}',
			config : {
				handler : pageController.listingById
			}
		},
	];
}();
