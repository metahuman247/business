"use strict";
//  NPM MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SCHEMAS
function CompanyValidate(){};
CompanyValidate.prototype = (function(){
	return {
		find : {
			query: (function query() {
				var companySchema = new models.Company().schema;
				return {
				};
			})()
		},
		refByID: {
			params: (function params() {
				var companySchema = new models.Company().schema;
				return {
					company_id: companySchema.company_id.required(),
				};
			})()
		},
		insert: {
			payload: (function payload() {
				var companySchema = new models.Company().schema;
				return {

				};
			})()
		},
		update: (function update() {
			var companySchema = new models.Company().schema;
			return {
				params: {
					company_id: companySchema.company_id.required()
				},
				payload: {

				}
			}
		})()
	};
})();
module.exports = new CompanyValidate();
