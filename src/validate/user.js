"use strict";
//  NPM MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SCHEMAS
function UserValidate(){};
UserValidate.prototype = (function(){
	return {
		find : {
			query: (function query() {
				var userSchema = new models.User().schema;
				return {
					user_id: userSchema.user_id
				};
			})()
		},
		refByID: {
			params: (function params() {
				var userSchema = new models.User().schema;
				return {
					user_id: userSchema.user_id.required(),
				};
			})()
		},
		insert: {
			payload: (function payload() {
				var userSchema = new models.User().schema;
				return {
					auth_email:    userSchema.auth_email.required(),
					auth_username: userSchema.auth_username.required(),
					auth_password: userSchema.auth_password.required()
				};
			})()
		},
		update: (function update() {
			var userSchema = new models.User().schema;
			return {
				params: {
					user_id: userSchema.user_id.required()
				},
				payload: {
					auth_email: userSchema.auth_email,
					auth_username: userSchema.auth_username,
					auth_password: userSchema.auth_password
				}
			}
		})()
	};
})();
module.exports = new UserValidate();
